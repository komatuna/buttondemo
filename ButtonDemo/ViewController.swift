//
//  ViewController.swift
//  ButtonDemo
//
//  Created by Hayato Komatsuzaki on 2020/10/11.
//

import UIKit

class ViewController: UIViewController {
    let SW = UIScreen.main.bounds.size.width
    let SH = UIScreen.main.bounds.size.height
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .lightGray
        
        let redButton: UIButton = UIButton()
        redButton.frame.size = CGSize(width: SW * 0.8, height: SH * 0.1)
        let spaceHeight = (SH - redButton.frame.size.height) / 5
        redButton.center = CGPoint(x: SW / 2, y: spaceHeight)
        redButton.backgroundColor = .white
        redButton.setTitle("Red", for: .normal)
        redButton.setTitleColor(.red, for: .normal)
        redButton.tag = 0
        redButton.addTarget(self, action: #selector(pushButtons(sender:)), for: .touchUpInside)
        view.addSubview(redButton)
        
        let blueButton: UIButton = UIButton()
        blueButton.frame.size = CGSize(width: SW * 0.8, height: SH * 0.1)
        blueButton.center = CGPoint(x: SW / 2, y: redButton.frame.origin.y + redButton.frame.size.height + spaceHeight)
        blueButton.backgroundColor = .white
        blueButton.setTitle("Blue", for: .normal)
        blueButton.setTitleColor(.blue, for: .normal)
        blueButton.tag = 1
        blueButton.addTarget(self, action: #selector(pushButtons(sender:)), for: .touchUpInside)
        view.addSubview(blueButton)
        
        let greenButton: UIButton = UIButton()
        greenButton.frame.size = CGSize(width: SW * 0.8, height: SH * 0.1)
        greenButton.center = CGPoint(x: SW / 2, y: blueButton.frame.origin.y + blueButton.frame.size.height + spaceHeight)
        greenButton.backgroundColor = .white
        greenButton.setTitle("Green", for: .normal)
        greenButton.setTitleColor(.green, for: .normal)
        greenButton.tag = 2
        greenButton.addTarget(self, action: #selector(pushButtons(sender:)), for: .touchUpInside)
        view.addSubview(greenButton)
        
        let yellowButton: UIButton = UIButton()
        yellowButton.frame.size = CGSize(width: SW * 0.8, height: SH * 0.1)
        yellowButton.center = CGPoint(x: SW / 2, y: greenButton.frame.origin.y + greenButton.frame.size.height + spaceHeight)
        yellowButton.backgroundColor = .white
        yellowButton.setTitle("Yellow", for: .normal)
        yellowButton.setTitleColor(.yellow, for: .normal)
        yellowButton.tag = 3
        yellowButton.addTarget(self, action: #selector(pushButtons(sender:)), for: .touchUpInside)
        view.addSubview(yellowButton)
    }
    
    @objc func pushButtons(sender: UIButton) {
        switch sender.tag {
        case 0:
            view.backgroundColor = .red
        case 1:
            view.backgroundColor = .blue
        case 2:
            view.backgroundColor = .green
        case 3:
            view.backgroundColor = .yellow
        default:
            view.backgroundColor = .lightGray
        }
    }

}

